package sheridan;


//Jacob Bruce
public class Celsius {

	public static int fromFahrenheit(int fahrenheit) {
		return (int) Math.ceil((5.0/9.0) * (fahrenheit - 32));
	}
	
}
