package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;


// Jacob Bruce - 991518971
public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		assertTrue("Result does not match expected", Celsius.fromFahrenheit(50) == 10);
	}
	
	@Test
	public void testFromFahrenheitExceptional() {
		assertFalse("Result does not match expected", Celsius.fromFahrenheit(50) == 50);
	}
	
	@Test
	public void testFromFahrenheitRegularBoundayIn() {
		assertTrue("Result does not match expected", Celsius.fromFahrenheit(53) == 12);
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		assertFalse("Result does not match expected", Celsius.fromFahrenheit(52) == 11);
	}

}
